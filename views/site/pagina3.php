<?php
        /**
         * Añadir codigo de JS despues de JQUERY en una vista
         */
        $this->registerJsFile(
            '@web/js/video.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );
?>


<div class="center-block embed-responsive embed-responsive-16by9">
    <video class="embed-responsive-item" controls autoplay>
        <source src="<?= Yii::getAlias("@web") . "/videos/video1.mp4"?>" type="video/mp4">
    </video>
</div>


<div class="center-block embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/lE6dL6yDGO4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



